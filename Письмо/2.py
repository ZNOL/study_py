import xlrd
import pymorphy2
from docxtpl import DocxTemplate

morph = pymorphy2.MorphAnalyzer()
"""
        try:
            if sex != 'ж':
                teacher_i = morph.parse(teacher.split()[1])[0].normal_form.capitalize()
                teacher_o = morph.parse(teacher.split()[2])[0].normal_form.capitalize()
            else:
                teacher_i = morph.parse(teacher.split()[1])[0].inflect({'femn', 'nomn'})[0].capitalize()
                teacher_o = morph.parse(teacher.split()[2])[0].inflect({'femn', 'nomn'})[0].capitalize()
            print(teacher_i, teacher_o)
        except TypeError:
            print(teacher)
            otchestva.add(teacher)
"""

with xlrd.open_workbook('Акция Письмо учителю 2021 (Ответы).xls') as book:
    sheet = book.sheet_by_name('для письма')

    cur_row = 1
    num_rows = 496
    while cur_row < num_rows:
        row = sheet.row(cur_row)

        doc = DocxTemplate('template2.docx')
        try:
            teacher = row[0].value
            school = row[1].value
            sex = row[3].value.lower()
            student = row[4].value
            teacher_i, teacher_o = row[5].value.split()

            if sex == 'м':
                obr = f'Уважаемый, {teacher_i} {teacher_o}!'
            else:
                obr = f'Уважаемая, {teacher_i} {teacher_o}!'

            content = {
                'fio': teacher,
                'school': school,
                'obr': obr,
                'student': student,
            }

            doc.render(content)

            if sex == 'м':
                doc.save(f"tmp/male/{teacher}-{student}.docx")
            else:
                doc.save(f"tmp/female/{teacher}-{student}.docx")


            print(teacher, teacher_o, teacher_i, school, student)
        except ValueError:
            print(row)
            break

        cur_row += 1

