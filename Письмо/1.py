from docxtpl import DocxTemplate


for idx in range(476):
    person = input().split()
    try:
        doc = DocxTemplate('template.docx')

        content = {
            'фамилия': person[0],
            'имя': person[1],
            'отчество': ' '.join(person[2:])
        }

        doc.render(content)

        doc.save(f"tmp/{person[0]}{person[1]}{person[2]}.docx")
    except Exception:
        print(person)

