from sys import stdin

n = int(input())
for i in range(n):
    s = next(stdin).strip().split()  # faster than input()
