from time import time

start_time = time()

print("--- %s seconds ---" % (time() - start_time))
