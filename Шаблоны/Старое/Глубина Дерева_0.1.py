from sys import setrecursionlimit
setrecursionlimit(1000000)


def height_form_stack(root, n, list_parents):
    global stack
    tmp = []
    for i in range(n):
        if list_parents[i] == root:
            tmp.append(i)
    stack += tmp
    for i in tmp:
        height_form_stack(i, n, list_parents)
    return True


def tree_height(root, n, list_parents):
    global dict_height
    height = 1
    for i in range(n):
        if list_parents[i] == root:
            if dict_height[i] != 0:
                height = max(height, 1 + dict_height[i])
            else:
                height = max(height, 1 + tree_height(i, n, list_parents))
    dict_height[root] = height
    return height


n = int(input())
m = [int(i) for i in input().split()]
dict_height = dict.fromkeys([i for i in range(n)], 0)
stack = [m.index(-1)]

height_form_stack(m.index(-1), n, m)

for i in stack[::-1]:
    tree_height(i, n, m)

print(dict_height[m.index(-1)])
