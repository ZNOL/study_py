def bin_nod(m, n):
	if m == 0:
		return n
	elif n == 0:
		return m
	elif n == m:
		return m
	elif m & 1 == 0 and n & 1 == 0:
		return 2 * bin_nod(m >> 1, n >> 1)
	elif m & 1 == 0 and n & 1 == 1:
		return bin_nod(m >> 1, n)
	elif m & 1 == 1 and n & 1 == 0:
		return bin_nod(m, n >> 1)
	elif (m & 1 == 1 and n & 1 == 1) and m > n:
		return bin_nod((m - n) >> 1, n)
	elif (m & 1 == 1 and n & 1 == 1) and n > m:
		return bin_nod((n - m) >> 1, m)


def bin_nod_2(m, n):
    nod = 1
    if m == 0:
        return n
    elif n == 0:
        return m
    elif n == m:
        return m
    elif n == 1 or m == 1:
        return 1
    while m != 0 and n != 0:
        if m & 1 == 0 and n & 1 == 1:
            m >>= 1
            continue
        if m & 1 == 1 and n & 1 == 0:
            n >>= 1
            continue
        if m & 1 == 0 or n & 1 == 0:
            nod <<= 1
            m >>= 1
            n >>= 1
            continue
        if m > n:
            tmp = m
            m = n
            n = tmp
        tmp = m
        m = (n - m) >> 1
        n = tmp
    if m == 0:
        return nod * n
    else:
        return nod * m


n = int(input())
c = list(map(int, input().split()))


a = bin_nod(c[0], c[1])
for i in range(2, n):
	a = bin_nod_2(a, c[i])

print(a)
