def NOD(a, b):
    a = abs(a)
    b = abs(b)
    while a != 0 and b != 0:
        if a > b:
            a = a % b
        else:
            b = b % a
    return a + b


def otr(a):
    return a < 0


h1, z1, h2, z2 = map(int, input().split())

k = z1 * z2 // NOD(z1, z2)  # общий знаменатель

f = k // z1
s = k // z2

h1 = h1 * f  # 1 числитель
h2 = h2 * s  # 2 числитель

HH = h1 + h2  # числитель после сложения

a = 0  # будущая целая часть дроби

# print('HH =', HH, 'K =', k)
# print(NOD(HH, k))

if HH == 0:
    print(0)
elif otr(HH):
    z = NOD(HH, k)
    if abs(HH) > k:
        while abs(HH) >= k:
            HH = abs(HH) - k
            a += 1
        if HH == 0:
            print(a * -1)
        else:
            if z != 1:
                while z != 1:
                    HH = HH // z
                    k = k // z
                    z = NOD(HH, k)
            print(a * -1, '' + str(HH) + '/' + str(k) + '')
    else:
        if z != 1:
            while z != 1:
                HH = HH // z
                k = k // z
                z = NOD(HH, k)
            print('' + str(HH) + '/' + str(k) + '')
        else:
            print('' + str(HH) + '/' + str(k) + '')
else:
    z = NOD(HH, k)
    if HH >= k:
        while HH >= k:
            HH -= k
            a += 1
        if HH == 0:
            print(a)
        else:
            if z != 1:
                while z != 1:
                    HH = HH // z
                    k = k // z
                    z = NOD(HH, k)
            print(a, '' + str(HH) + '/' + str(k) + '')
    else:
        if z != 1:
            while z != 1:
                HH = HH // z
                k = k // z
                z = NOD(HH, k)
            print('' + str(HH) + '/' + str(k) + '')
        else:
            print('' + str(HH) + '/' + str(k) + '')
