def super(a,n,k,b):
    ud=1
    j=0
    i=1
    while i<n:
        while i<n and b[i]-b[j]<a:
            i+=1
        if i<n and b[i]-b[j]>=a:
            ud+=1
            j=i
            i+=1
                
    if ud>=k:
        return True
    else:
        return False


z = 1
max = 10**9
bin = 1

n, k = map(int, input().split())
b = input().split()

for i in range(n):
    b[i]=int(b[i])

while -z+max>1:
    bin = (z+max)//2
    if super(bin, n, k, b):
        z = bin
    else:
        max = bin

print(z)
