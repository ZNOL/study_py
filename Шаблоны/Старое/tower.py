def towers(n,f,l):
	global ans
	if n == 1:
		ans.append([n, f, l])
		return ans
	else:
		s = 6 - l - f
		towers(n-1, f, s)
		ans.append([n, f, l])
		towers(n-1, s, l)
		return ans


ans = []
n = int(input())
f = 1
l = 3

for i in towers(n, f, l):
	print(*i)

