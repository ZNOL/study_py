# coding=UTF-8
from itertools import combinations  # для комбинаций всего
from heapq import *

'''
from sys import stdin
n = int(input())
for i in range(n):
    s = next(stdin).strip().split()  # faster than input()
'''


def factor(n):  # простые множители числа n
    m = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            m.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        m.append(n)
    return m


def per_pizano(m):  # последовательность Фибоначи по модулю m
    x = [0, 1, 1]
    for i in range(3, 1000000000):
        a = (x[i - 2] + x[i - 1]) % m
        if a == x[-1] == 1 and x[-2] == 0:
            length = len(x)
            x.pop(length - 1)
            x.pop(length - 2)
            break
        x.append(a)
    return x


def comb(s):  # всевозможные комбинации s
    return [''.join(l) for i in range(len(s)) for l in combinations(s, i + 1)]


def heap_sort(sequence):  # сортировка кучей
    def swap_items(index1, index2):
        if sequence[index1] < sequence[index2]:
            sequence[index1], sequence[index2] = sequence[index2], sequence[index1]

    def sift_down(parent, limit):
        while True:
            child = (parent + 1) << 1  # То же, что и parent * 2 + 2
            if child < limit:
                if sequence[child] < sequence[child - 1]:
                    child -= 1
                swap_items(parent, child)
                parent = child
            else:
                break
    # Тело функции heap_sort
    length = len(sequence)
    # Формирование первичной пирамиды
    for index in range((length >> 1) - 1, -1, -1):
        sift_down(index, length)
    # Окончательное упорядочение
    for index in range(length - 1, 0, -1):
        swap_items(index, 0)
        sift_down(0, index)


def del_char(a, b):  # удаляет все символы b из a
    translation_table = dict.fromkeys(map(ord, b), None)
    return a.translate(translation_table)


def okr(a):  # округление до 100-ых
    if a == int(a):
        return ''+str(int(a))+'.000'
    else:
        a, b = str(a).split('.')
        if len(b) == 1:
            b += '00'
        elif len(b) == 2:
            b += '00'
        elif len(b) == 3:
            return '' + a + '.' + b + ''
        else:
            if 0 <= int(b[3]) <= 4:
                return ''+a+'.'+b[:3]+''
            else:
                b = b[:2] + str(int(b[2]) + 1)
                return ''+a+'.'+b+''


def slag(n):  # разложение на натуральные слагаемые
    ans = []
    z = 1
    while n != 0:
        if ans:
            if 0 < n - z <= ans[-1] + 1:
                ans.append(n)
                n = 0
            else:
                n -= z
                ans.append(z)
        else:
            if n == 2:
                ans.append(2)
                n = 0
            else:
                ans.append(1)
                n -= z
        z += 1
    return ans


def check_brackets(string):  # проверка правильности скобок
    stack = []  # стек с самимы скобками
    last_index = []  # стек с индексами скобок
    list_brackets = {'(', ')', '[', ']', '{', '}'}
    dict_brackets = {
        '(': ')',
        '[': ']',
        '{': '}'
    }

    for i in range(len(string)):
        if string[i] not in list_brackets:
            continue
        elif string[i] in dict_brackets.keys():
            stack.append(string[i])
            last_index.append(i + 1)
        else:
            if not stack:
                return i + 1
            elif dict_brackets.get(stack[-1]) == string[i]:
                del stack[-1]
                del last_index[-1]
            else:
                return i + 1

    if not stack:
        return True
    else:
        return last_index[-1]


def tree_height(n, list_parents):  # глубина дерева по списку родителей -> списку смежности
    root = list_parents.index(-1)
    list_parents[root] = root
    contiguity_list = [[] for _ in range(n)]
    for i in range(n):
        if list_parents[i] != i:
            contiguity_list[list_parents[i]].append(i)

    height = 1
    turn = [*contiguity_list[root]]
    add_turn = []
    while turn:
        for i in range(len(turn)):
            if contiguity_list[turn[i]]:
                add_turn.append(turn[i])
        turn.clear()
        for i in add_turn:
            turn += contiguity_list[i]
        add_turn.clear()
        height += 1

    return height


def heap_sort_2(iterable):
    h = []
    for value in iterable:
        heappush(h, value)
    return [heappop(h) for i in range(len(h))]


''' Очередь с приоритетом
pq = []                         # list of entries arranged in a heap
entry_finder = {}               # mapping of tasks to entries
REMOVED = '<removed-task>'      # placeholder for a removed task
counter = itertools.count()     # unique sequence count
'''

def add_task(task, priority=0):
    'Add a new task or update the priority of an existing task'
    if task in entry_finder:
        remove_task(task)
    count = next(counter)
    entry = [priority, count, task]
    entry_finder[task] = entry
    heappush(pq, entry)

def remove_task(task):
    'Mark an existing task as REMOVED.  Raise KeyError if not found.'
    entry = entry_finder.pop(task)
    entry[-1] = REMOVED

def pop_task():
    'Remove and return the lowest priority task. Raise KeyError if empty.'
    while pq:
        priority, count, task = heappop(pq)
        if task is not REMOVED:
            del entry_finder[task]
            return task
    raise KeyError('pop from an empty priority queue')
'''


print(factor(10))

print(per_pizano(2))

print(comb('123'))

x = [i for i in range(10, -1, -1)]
heap_sort(x)
print(x)

print(del_char('Дима - не lox', [' ', 'н', 'е']))

print(okr(123.4567))

print(slag(100))

print('brackets')
print(check_brackets('(()[([][]())[()][()][][]])([])()'))

print(tree_height(10, [8, -1, 1, 1, 2, 2, 3, 3, 6, 6]))

print(heap_sort_2([i for i in range(10, -1, -1)]))
