def f(n):
    ans = []
    for i in range(2, int(n ** 0.5) + 1):
        if n % i == 0:
            j = n // i
            if i != j:
                ans.append(i)
                ans.append(j)
            else:
                ans.append(i)
    ans.sort()
    return ans


for x in range(210235, 210300 + 1):
    tmp = f(x)
    if len(tmp) == 4:
        print(x, tmp)
