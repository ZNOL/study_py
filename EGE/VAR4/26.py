with open("26.txt", 'r') as file:
    s, n = map(int, file.readline().strip().split())
    m = []
    for i in range(n):
        t = int(file.readline().strip())
        m.append(t)

m.sort()
ans = 0
tmp = 0
last = -1
for i in range(n):
    if tmp + m[i] <= s:
        ans += 1
        tmp = tmp + m[i]
        last = m[i]
    else:
        break

tmp = tmp - last
for i in range(n - 1, -1, -1):
    if tmp + m[i] <= s:
        last = m[i]
        break

print(ans, last)

