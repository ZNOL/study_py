with open("24_2.txt", 'r') as file:
    s = file.readline()
n = len(s)

ans = -1
tmp = 0
for i in range(n - 1):
    if s[i] != s[i + 1]:
        tmp += 1
    else:
        tmp += 1
        if ans < tmp:
            ans = tmp
        tmp = 0
if tmp != 0:
    tmp += 1
    if ans < tmp:
        ans = tmp

print(ans)
