with open('24.txt', 'r') as file:
    s = file.readline().strip()
n = len(s)

k = 0
ans = -1
tmp = 0
for i in range(n):
    if s[i] == "X":
        t = 0
    elif s[i] == "Y":
        t = 1
    else:
        t = 2

    if t == k:
        tmp += 1
        k = (k + 1) % 3
    else:
        if tmp > ans:
            ans = tmp
        tmp = 0
        k = 0
if tmp > ans:
    ans = tmp

print(ans)


