with open("24.txt", 'r') as file:
    s = file.readline().strip()
n = len(s)

m = [0] * 26
for i in range(1, n):
    if s[i - 1] == "A":
        idx = ord(s[i]) - 65
        m[idx] += 1

mx = m[0]
ans = 'A'
for i in range(1, len(m)):
    if m[i] > mx:
        mx = m[i]
        ans = chr(i + 65)
print(mx, ans)
