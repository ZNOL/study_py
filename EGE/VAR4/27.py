with open('27-B.txt') as file:
    n = int(file.readline().strip())

    mindif = 1000000000
    s = 0
    for i in range(n):
        a, b = map(int, file.readline().strip().split())
        s += max(a, b)
        if a % 3 != b % 3:
            tmp = max(a, b) - min(a, b)
            mindif = min(mindif, tmp)
print(mindif, s % 3)
if s % 3 == 0:
    s -= mindif
    print(s, s % 3)