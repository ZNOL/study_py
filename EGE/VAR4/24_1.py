with open('24.txt', 'r') as file:
    s=file.readline().strip()
n=len(s)

ans = -1
tmp = 0
for i in range(n):
    if s[i] == "X":
        tmp+=1
    else:
        if tmp>ans:
            ans=tmp
        tmp=0

if tmp>ans:
    ans=tmp
print(ans)