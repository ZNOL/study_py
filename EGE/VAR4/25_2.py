def f(n):
    m=[]
    for i in range(1, int(n**0.5)+1):
        if n%i==0:
            j=n//i
            if i!=j:
                if i%2==1:
                    m.append(i)
                if j%2==1:
                    m.append(j)
            else:
                if i%2==1:
                    m.append(i)
    return m


for x in range(95632, 95650+1):
    tmp = f(x)
    if len(tmp)==6:
        tmp.sort()
        print(tmp, max(tmp))
