def is_win(x, y):
    c = 2 * max(x, y) + min(x, y)
    if c >= 77:
        return True
    else:
        return False


def game_20(k, x, y):
    if k == 1:
        if is_win(x, y):
            return False
        tmp1 = game_20(k + 1, x + 1, y)
        tmp2 = game_20(k + 1, x, y + 1)
        tmp3 = game_20(k + 1, 2 * x, y)
        tmp4 = game_20(k + 1, x, 2 * y)
        return tmp1 or tmp2 or tmp3 or tmp4
    elif k == 2:
        if is_win(x, y):
            return False
        tmp1 = game_20(k + 1, x + 1, y)
        tmp2 = game_20(k + 1, x, y + 1)
        tmp3 = game_20(k + 1, 2 * x, y)
        tmp4 = game_20(k + 1, x, 2 * y)
        return tmp1 and tmp2 and tmp3 and tmp4
    elif k == 3:
        if is_win(x, y):
            return True
        else:
            return False


def game_21(k, x, y):
    if k == 1 or k == 3:
        if is_win(x, y):
            return False
        tmp1 = game_21(k + 1, x + 1, y)
        tmp2 = game_21(k + 1, x, y + 1)
        tmp3 = game_21(k + 1, 2 * x, y)
        tmp4 = game_21(k + 1, x, 2 * y)
        return tmp1 and tmp2 and tmp3 and tmp4
    elif k == 2:
        if is_win(x, y):
            return True
        tmp1 = game_21(k + 1, x + 1, y)
        tmp2 = game_21(k + 1, x, y + 1)
        tmp3 = game_21(k + 1, 2 * x, y)
        tmp4 = game_21(k + 1, x, 2 * y)
        return tmp1 or tmp2 or tmp3 or tmp4
    elif k == 4:
        if is_win(x, y):
            return True
        else:
            return False


for s in range(1, 69 + 1):
    if game_21(1, 7, s):
        print(s)
