alf = ['П', 'О', 'Л', 'И', 'Н', 'А']


def check(word):
    gl = ['О', 'А', 'И']
    sogl = ['П', 'Л', 'Н']

    for burva in alf:
        if word.count(burva) != 1:
            return False

    for i in range(5):
        first = word[i]
        second = word[i + 1]
        if not(first in gl and second in sogl or first in sogl and second in gl):
            return False

    return True


ans = 0
for a in alf:
    for b in alf:
        for c in alf:
            for d in alf:
                for e in alf:
                    for f in alf:
                        word = a + b + c + d + e + f
                        if check(word) == 1:
                            ans = ans + 1

print(ans)
