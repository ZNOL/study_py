def func(x):
    x = str(x)
    first = int(x[0]) + int(x[2]) + int(x[4])
    second = int(x[1]) + int(x[3])
    if first <= second:
        ans = int(str(first) + str(second))
    else:
        ans = int(str(second) + str(first))
    return ans


for x in range(10000, 100000):
    if func(x) == 723:
        print(x, func(x))
