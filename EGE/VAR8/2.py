def dectofib(x):
    ans = []
    k = 2
    while x:
        ans.append(str(x % k))
        x //= k

        k += 1
    return int(''.join(ans[::-1]))


def fibtodec(x):
    ans = 0
    f = 1
    k = 2
    while x:
        last = x % 10
        ans += last * f
        
        f = f * k
        k += 1
        
        x //= 10

    return ans


x = int(input())

print('Ф -> Д', dectofib(x), fibtodec(dectofib(x)))

print('Д -> Ф', fibtodec(x), dectofib(fibtodec(x)))
