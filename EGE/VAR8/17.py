d2 = []
d5 = []
d10 = []
d = []
with open('17.txt', 'r', encoding='utf-8') as file:
    for line in file.readlines():
        dig = int(line.strip())

        if dig % 10 == 0:
            d10.append(dig)
        elif dig % 2 == 0:
            d2.append(dig)
        elif dig % 5 == 0:
            d5.append(dig)
        else:
            d.append(dig)

ans5 = -1
ans1 = max(d2) + max(d5)
ans2 = max(d10) + max(d2)
ans3 = max(d10) + max(d5)
ans4 = max(d10) + max(d)

if len(d10) >= 2:
    d10.sort()
    ans5 = d10[-1] + d10[-2]

ans = max(ans1, ans2, ans3, ans4, ans5)


count10 = len(d2) * len(d5) + len(d10) * (len(d10) - 1) // 2 + len(d2) * len(d10) + len(d5) * len(d10) + len(d10) * len(d)

print(count10, ans)  # c(2, n) = n * (n - 1) / 2