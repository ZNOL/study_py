from decimal import Decimal

v = []
with open('18.txt', 'r') as file:
    for line in file.readlines():
        dig = Decimal(line.strip().replace(',', '.'))
        v.append(dig)

print(len(v))
dp = [Decimal('-100000000000000000000')] * 501
mx = v[0]
dp[0] = v[0]
for i in range(2, 500):
    try:
        if abs(v[i] - v[i - 1]) <= 8:
            dp[i] = max(dp[i], dp[i - 1] + v[i], v[i])
        else:
            dp[i] = max(dp[i], v[i])

    except IndexError:
        print(i)

print(max(dp))

