def func(x):
    a = 0
    b = 1
    while x > 0:
        if x % 2 > 0:
            a += x % 12
        else:
            b *= x % 12
        x = x // 12
    return (a, b)


for i in range(100000, 0, -1):
    if func(i) == (2, 10):
        print(i)
        break


