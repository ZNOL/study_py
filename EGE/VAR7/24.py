with open('24.txt') as file:
    s = file.read().strip()
n = len(s)

last = -1
ans = 0

for i in range(n - 4):  # xzzy
    if s[i] == 'X' and s[i + 1] == s[i + 2] == 'Z' and s[i + 3] == 'Y':
        if last != -1:
            tmp = i - last + 2
            ans = max(tmp, ans)
        last = i

print(ans)

