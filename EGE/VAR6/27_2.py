m = []
with open('27-B.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        a = int(file.readline().strip())
        if a % 17 == 0:
            print('have')
        m.append(a)

ans = 0
for i in range(n):
    for j in range(n):
        a = m[i]
        b = m[j]
        if (a * b) % 17 == 0 and abs(i - j) >= 3:
            ans += 1

print(ans)
print(ans / 2)
