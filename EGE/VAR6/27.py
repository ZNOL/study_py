def rec(idx):
    global mx
    if idx >= n:
        s = 0
        for i in range(n):
            cur = m[i]
            s += seq[i][cur]

        if s % 3 != 0 and mx < s:
            mx = s
        return

    m[idx] = 0
    rec(idx + 1)
    m[idx] = 1
    rec(idx + 1)


mx = -1
seq = []
with open('27.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        a, b = map(int, file.readline().strip().split())
        seq.append([a, b])

m = [0] * n
rec(0)
print(mx, mx % 3)
