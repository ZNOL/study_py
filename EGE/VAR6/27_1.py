s = 0
mindif = 10000000000000000000
with open('27_B.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        a, b = map(int, file.readline().strip().split())
        s += max(a, b)
        if a % 3 != b % 3:
            tmp = abs(a - b)
            if tmp < mindif:
                mindif = tmp

if s % 3 == 0:
    s -= mindif
    print(s, mindif)
else:
    print('ezz', s)
