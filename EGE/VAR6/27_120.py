m = []
with open('27_120.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        a = int(file.readline().strip())
        m.append(a)

mx = -1
ans = [-1, -1]
for i in range(n):
    for j in range(i + 1, n):
        ai = m[i]
        aj = m[j]
        tmp = ai + aj
        if ai > aj and tmp % 120 == 0:
            if tmp > mx:
                mx = tmp
                ans = [ai, aj]

print(mx)
print(ans)
