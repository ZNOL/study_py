for x in range(1, 1000):
    bi=bin(x)[2:]
    s = 0
    for i in range(2):
        for di in bi:
            s += int(di)
        bi += str(s % 2)
        s = 0
    if int(bi, 2) > 63:
        print(x)
        break

for x in range(1, 1000):
    tmp = x
    for i in range(2):
        s = 0
        c = tmp
        while c != 0:
            s += c % 2
            c = c // 2
        if s % 2 == 0:
            tmp = tmp * 2
        else:
            tmp = tmp * 2 + 1
    if tmp > 63:
        print(x)
        break