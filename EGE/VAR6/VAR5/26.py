with open('26.txt') as file:
    n, k, m = map(int, file.readline().strip().split())
    seq = []
    for i in range(n):
        t = file.readline().strip()
        seq.append(int(t))

seq.sort(reverse=True)
#seq = seq.sort()[::-1]

print(seq[:k][-1])
print(seq[k:m + k][-1])
