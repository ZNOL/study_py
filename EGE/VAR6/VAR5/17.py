ans=0
maxi=-10000000
for x in range(-9563, -3102 + 1):
    if x%10!=8 and x%7==0 and x%11!=0 and x%23!=0:
        ans+=1
        maxi=max(maxi,x)
print(ans, maxi)