with open('27_B.txt') as file:
    n = int(file.readline().strip())

    e = 100000000000
    e11 = 100000000000

    o = 100000000000
    o11 = 100000000000

    for i in range(n):
        t = int(file.readline().strip())
        if t % 2 == 0:
            if t % 11 == 0 and t < e11:
                e11 = t
            elif t % 11 != 0 and t < e:
                e = t
        else:
            if t % 11 == 0 and t < o11:
                o11 = t
            elif t % 11 != 0 and t < o:
                o = t

if e + e11 < o + o11:
    print(e, e11)
else:
    print(o, o11)
