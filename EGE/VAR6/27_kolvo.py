m = []
m2 = []
m13 = []
m26 = []

with open('27989_B.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        a = int(file.readline().strip())
        if a % 26 == 0:
            m26.append(a)
        elif a % 13 == 0:
            m13.append(a)
        elif a % 2 == 0:
            m2.append(a)
        else:
            m.append(a)

l = len(m)
l2 = len(m2)
l13 = len(m13)
l26 = len(m26)

ans = 0
ans += l26 * l
ans += l26 * l2
ans += l26 * l13
ans += l2 * l13
ans += l26 * (l26 - 1) // 2

print(ans)

