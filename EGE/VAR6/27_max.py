m2 = -1
m7 = -1
m14 = -1
m = -1
with open('27_max2.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        a = int(file.readline().strip())
        if a % 14 == 0:
            if a > m14:
                if m14 > m:
                    m = m14
                m14 = a
            else:
                if a > m:
                    m = a
        elif a % 7 == 0:
            print('!!!')
            if a > m7:
                if m7 > m:
                    m = m7
                m7 = a
            else:
                if a > m:
                    m = a
        elif a % 2 == 0:
            if a > m2:
                if m2 > m:
                    m = m2
                m2 = a
            else:
                if a > m:
                    m = a
        else:
            if a > m:
                m = a

print(m14, m2, m7, m)

tmp1 = m14 * m
tmp2 = m14 * m2
tmp3 = m14 * m7
tmp4 = m2 * m7
print(max(tmp1, tmp2, tmp3, tmp4))
