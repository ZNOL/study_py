with open('24.txt') as file:
    s = file.read().strip()
n = len(s)

mx = -1
last = -1
for i in range(n - 4):
    if s[i] == 'X' and s[i + 1] == s[i + 2] == 'Z' and s[i + 3] == 'Y':
        if last != -1:
            tmp = i - last + 2
            if mx < tmp:
                mx = tmp
        last = i

print(mx)
