ans = 0
mx = -1
mn = -1

for x in range(2476, 7857 + 1):
    if x % 2 == 0 and x % 8 != 0 and x % 1000 // 100 <= 7:
        ans += 1
        if mn == -1:
            mn = x
        mx = x

print(ans, (mx + mn) / 2)
