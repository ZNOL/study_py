m = set()
mdel = set()
with open('27_B.txt') as file:
    n = int(file.readline().strip())
    for i in range(n):
        t = int(file.readline().strip())
        if t % 37 == 0:
            mdel.add(t)
        else:
            m.add(t)

m = sorted(list(m))
mdel = sorted(list(mdel))

mx = -1
ans = []
for x in m:
    for y in mdel:
        if abs(x - y) % 2 == 1:
            if x + y > mx:
                mn = x + y
                ans.clear()
                ans.append((min(x, y), max(x, y)))
            elif x + y == mx:
                ans.append((min(x, y), max(x, y)))

print(ans)