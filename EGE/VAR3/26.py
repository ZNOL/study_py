cat = []
with open('26.txt') as file:
    n, k, m = map(int, file.readline().strip().split())
    for i in range(n):
        tmp = int(file.readline().strip())
        cat.append(tmp)

cat.sort()

print(sum(cat[:k]) / k)
print(cat[-m:])