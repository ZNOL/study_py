def foo(x):
    a = 1
    b = 0
    while x > 0:
        b += 1
        if x % 2 == 0:
            a = a * (x % 8)
        x //= 8
    return a, b

for x in range(1000, -1, -1):
    if foo(x) == (2, 3):
        print(x)
        break