def counter(n):
    ans = 0
    for i in range(1, int(n ** 0.5) + 1):
        if n % i == 0:
            j = n // i
            if i != j:
                ans += 2
            else:
                ans += 1
    return ans

m = []
mx = -1
for x in range(59999, 64000 + 1):
    tmp = counter(x)
    if mx < tmp:
        m.clear()
        m.append(x)
        mx = tmp
    elif mx == tmp:
        m.append(x)

print(mx)
print(max(m), m)
