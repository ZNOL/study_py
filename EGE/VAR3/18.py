n = int(input())
m = [list(map(int, input().split())) for i in range(n)]

dp_min = [[1000000000000] * (n + 1) for i in range(n + 1)]
dp_max = [[-1000000000000] * (n + 1) for i in range(n + 1)]

dp_max[0][1] = dp_min[0][1] = 0
for i in range(1, n + 1):
    for j in range(1, n + 1):
        dp_max[i][j] = max(dp_max[i][j], dp_max[i - 1][j], dp_max[i][j - 1])
        dp_min[i][j] = min(dp_min[i][j], dp_min[i - 1][j], dp_min[i][j - 1])
        if m[i - 1][j - 1] % 2 == 0:
            dp_max[i][j] += m[i - 1][j - 1]
            dp_min[i][j] += m[i - 1][j - 1]

print(dp_max[n][n], dp_min[n][n])
