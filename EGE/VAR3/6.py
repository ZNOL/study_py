def foo(s):
    for k in range(3, 9):
        s += k
    return s

for i in range(1000):
    if foo(i) > 100:
        print(i)
        break