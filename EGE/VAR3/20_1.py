def is_win(x, y):
    tmp1=x*2+y
    tmp2=y*2+x
    if tmp1>=63 or tmp2>=63:
        return True
    else:
        return False

def game_21(x, y, k):
    if k==1 or k==3:
        if is_win(x, y):
            return False
        f1=game_21(x+1, y, k+1)
        f2=game_21(x*2, y, k+1)
        f3=game_21(x, y+1, k+1)
        f4=game_21(x, y*2, k+1)
        return f1 and f2 and f3 and f4
    if k==2:
        if is_win(x, y):
            return True
        f1 = game_21(x + 1, y, k + 1)
        f2 = game_21(x * 2, y, k + 1)
        f3 = game_21(x, y + 1, k + 1)
        f4 = game_21(x, y * 2, k + 1)
        return f1 or f2 or f3 or f4
    if k==4:
        if is_win(x, y):
            return True
        else:
            return False

for s in range(1, 57+1):
    if game_21(5, s, 1):
        print(s)