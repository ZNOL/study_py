def is_win(x, y):
    if x > y:
        x, y = y, x
    return x + 2 * y >= 63


def game_20(x, y, k = 1):
    if k == 1:
        if is_win(x, y):
            return False
        f1 = game_20(x + 1, y, k + 1)
        f2 = game_20(x, y + 1, k + 1)
        f3 = game_20(2 * x, y, k + 1)
        f4 = game_20(x, 2 * y, k + 1)
        return f1 or f2 or f3 or f4
    elif k == 2:
        if is_win(x, y):
            return False
        f1 = game_20(x + 1, y, k + 1)
        f2 = game_20(x, y + 1, k + 1)
        f3 = game_20(2 * x, y, k + 1)
        f4 = game_20(x, 2 * y, k + 1)
        return f1 and f2 and f3 and f4
    else:
        return is_win(x, y)


def game_21(x, y, k = 1):
    if k == 1 or k == 3:
        if is_win(x, y):
            return False
        f1 = game_21(x + 1, y, k + 1)
        f2 = game_21(x, y + 1, k + 1)
        f3 = game_21(2 * x, y, k + 1)
        f4 = game_21(x, 2 * y, k + 1)
        return f1 and f2 and f3 and f4
    elif k == 2:
        if is_win(x, y):
            return True
        f1 = game_21(x + 1, y, k + 1)
        f2 = game_21(x, y + 1, k + 1)
        f3 = game_21(2 * x, y, k + 1)
        f4 = game_21(x, 2 * y, k + 1)
        return f1 or f2 or f3 or f4
    else:
        return is_win(x, y)


for s in range(1, 57 + 1):
    if game_21(5, s):
        print(s)
