def foo(n):
    x = bin(n)[2:]
    for _ in range(2):
        x += str(sum(map(int, x)) % 2)
    return int(x, 2)


for n in range(0, 1000):
    if foo(n) > 125:
        print(n)
        break