with open('24.txt') as file:
    s = file.readline().strip()

n = len(s)
mx = -1
tmp = 1 if s[0] == 'X' else 0
for i in range(1, n):
    if s[i] == 'X':
        tmp += 1
    else:
        mx = max(mx, tmp)
        tmp = 0
mx = max(mx, tmp)

print(mx)
    