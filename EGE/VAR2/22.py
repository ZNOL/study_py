def foo(x):
    l = 0
    m = 0
    while x > 0:
        m += 1
        if x % 2 != 0:
            l += 1
        x //= 2
    return (l, m)


for x in range(1000):
    if foo(x) == (5, 8):
        print(x)
        break