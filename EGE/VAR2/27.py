with open('27_B.txt', 'r') as file:
    n = int(file.readline().strip())

    ans = 0
    mindif = 100000000000
    for i in range(n):
        a, b, c = map(int, file.readline().strip().split())

        if a > b:
            a, b = b, a
        if b > c:
            b, c = c, b
        if a > b:
            a, b = b, a

        tmp1 = c - a
        tmp2 = c - b

        if tmp1 % 109 != 0 and tmp2 % 109 != 0:
            mindif = min(mindif, tmp1, tmp2)
        elif tmp1 % 109 != 0 and tmp2 % 109 == 0:
            mindif = min(mindif, tmp1)
        elif tmp1 % 109 == 0 and tmp2 % 109 != 0:
            mindif = min(mindif, tmp2)

        ans += c

if ans % 109 == 0:
    print('NOO')
    if (ans - mindif) % 109 != 0:
        print(ans - mindif)
    else:
        print('N' + '0' * 100)
else:
    print(ans)