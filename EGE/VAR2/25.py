from math import sqrt

def foo(x):
    for i in range(2, int(sqrt(x)) + 1):
        if x % i == 0:
            f = i
            s = x // f
            if f != s:
                m = f + s
                break
    else:
        m = 0
    return m

ans = []
i = 452021 + 1
while True:
    tmp = foo(i)
    if tmp % 7 == 3:
        ans.append([i, tmp])

    if len(ans) == 5:
        break

    i += 1

for i in ans:
    print(i)
