x = 7 * 512 ** 120 - 6 * 64 ** 100 + 8 ** 210 - 255

ans = 0
while x:
    if x % 8==0:
        ans += 1
    x //= 8

print(ans)