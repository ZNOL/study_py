def foo(s):
    n = 1
    while s < 47:
        s += 4
        n *= 2
    return n

for i in range(100, -1, -1):
    if foo(i) == 64:
        print(i)
        break