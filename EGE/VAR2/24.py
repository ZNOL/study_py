with open('24.txt', 'r') as file:
    s = file.readline().strip()
# s = input()


# def check(size):
#     global s

n = len(s)
ans = -1
idx1 = 0
idx2 = 1
while True:
    if 'XZZY' not in s[idx1:idx2] and idx2 < n:
        idx2 += 1
    else:
        if 'XZZY' not in s[idx1:idx2 - 1] and idx1 != idx2 - 1:
            ans = max(ans, idx2 - 1 - idx1)
        idx1 = idx2
        idx2 = idx1 + 1
        if idx2 > n:
            break

print(ans)
