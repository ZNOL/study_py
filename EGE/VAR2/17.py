ans = 0
f = -1
for x in range(16015, 48989 + 1):
    if (x % 7 == 0 or x % 11 == 0) and x % 9 != 0 and x % 12 != 0 and x % 13 != 0:
        ans += 1
        if f == -1:
            f = x
print(ans, f)