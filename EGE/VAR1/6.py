def foo(s, x):
    s = 100 * s + x
    n = 1
    while s < 2021:
        s += 5 * n
        n += 1
    return n


ans = 100000000000000
for s in range(1, 10000):
    for x in range(1, 1000):
        if foo(s, x) == 15:
            ans = min(ans, x)

print(ans)
