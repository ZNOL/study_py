seq = [0 for i in range(10)]
s = set()

def foo():
    ans = 1
    for i in range(10):
        if seq[i] == 0:
            ans = 2 * ans
        else:
            ans = 2 * ans + 1
    return ans


def rec(idx):
    if idx == 10:
        s.add(foo())
        return

    seq[idx] = 0
    rec(idx + 1)
    seq[idx] = 1
    rec(idx + 1)


rec(0)
print(len(s))