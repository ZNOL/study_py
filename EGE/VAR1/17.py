def bar(x):
    s = sum(int(i) for i in str(x))
    return s

ans = 0
f = -1
for x in range(345678, 456789 + 1):
    if x % bar(x) == 0:
        if f == -1:
            f = x
        ans += 1

print(ans, f)
