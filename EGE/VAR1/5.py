def foo(n):
    if n % 2 == 0:
        n /= 2
    else:
        n -= 1

    if n % 3 == 0:
        n /= 3
    else:
        n -= 1

    if n % 5 == 0:
        n /= 5
    else:
        n -= 1

    return n

ans = []
for i in range(2, 100000):
    if foo(i) == 1:
        ans.append(i)

print(len(ans))
print(ans)
