x = 729 ** 6 - 3 ** 20 + 90

ans = 0
while x:
    if x % 9 == 0:
        ans += 1
    x //= 9

print(ans)
