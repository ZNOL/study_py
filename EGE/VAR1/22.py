def foo(x):
    a = 3 * x + 23
    b = 3 * x - 17
    while a != b:
        if a > b:
            a -= b
        else:
            b -= a
        if a < 20 or b < 20:
            break
    return a


for x in range(1, 100):
    if foo(x) == 20:
        print(x)
        break