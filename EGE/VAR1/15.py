def foo(x, A):
    return (not(x & 73 == 0) or (x & 28 == 0 or x & A != 0))


for a in range(0, 100):
    f = all(foo(x, a) for x in range(0, 1000))
    if f:
        print(a)