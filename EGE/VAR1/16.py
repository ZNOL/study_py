def f(n):
    if n == 0:
        return 0
    if n % 2 == 0:
        return f(n // 2)
    else:
        return 1 + f(n - 1)

ans = 0
for i in range(1, 500 + 1):
    if f(i) == 3:
        ans += 1

print(ans)
