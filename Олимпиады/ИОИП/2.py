n = int(input())
m = list(map(int, input().split()))
m2 = m[::]

ans1 = 0
ans2 = 0

mx = m[n - 1]
for i in range(n - 1):
    if m[i] >= mx:
        ans1 += 1
        m[i] = 'x'
for i in range(n):
    if m[i] != 'x':
        for j in range(n - 1, i, -1):
            if m[j] != 'x':
                if m[j] <= m[i]:
                    ans1 += 1
        break

mn = m2[0]
for i in range(n - 1, 0, -1):
    if m2[i] <= mn:
        m2[i] = 'x'
        ans2 += 1
for i in range(n - 1, -1, -1):
    if m2[i] != 'x':
        for j in range(0, i):
            if m2[j] != 'x':
                if m2[j] >= m2[i]:
                    ans2 += 1
        break

print(ans1, ans2)
print(min(ans1, ans2))




