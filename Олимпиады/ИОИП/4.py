from itertools import combinations


def is_road(i, j):
    while True:
        if j == i:
            return True
        if j == 1:
            return False
        j = p[j]


n, k = map(int, input().split())

z = {0, 1}
p = [0, 1]
for i in map(int, input().split()):
    p.append(i)
    z.add(i)

if n == 1 or k == n:
    print(n)
else:
    m = [[0] * (n + 1) for i in range(n + 1)]
    for i in range(1, n + 1):
        for j in range(i, n + 1):
            if i == j or is_road(i, j):
                m[i][j] = 1
                if is_road(j, i):
                    m[j][i] = 1
    ans = -1
    for i in combinations(range(2, n + 1), k):
        s = 0
        for j in range(1, n + 1):
            for x in range(k):
                if m[j][i[x]]:
                    s += 1
                    break
        if s > ans:
            ans = s
    print(ans)