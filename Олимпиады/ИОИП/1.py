from math import sqrt


def dist(x0, y0, x1, y1):
    return sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)


xs, ys, xt, yt = map(int, input().split())
n = int(input())
mn = 10000000.0
for i in range(n):
    xi, yi, ti = map(int, input().split())
    t = dist(xs, ys, xi, yi) + ti + dist(xi, yi, xt, yt)
    if t < mn:
        mn = t

print('{:.15f}'.format(mn))
