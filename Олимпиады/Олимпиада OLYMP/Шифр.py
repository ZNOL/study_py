mas = []
bl = True

f = open('input.txt')
for line in f:
    mas += [list(map(int, line.split()))]
f.close()

length = mas[0][0]
if length == 1:
    first = mas[1][0]
    second = mas[2][0]
else:
    first = mas[1]
    second = mas[2]


if length == 1:
    f = open('output.txt', 'w')
    f.write('YES' + '\n')
    f.write(str(first - second))
    f.close()
else:
    a = first[0] - second[0]
    for i in range(length):
        if first[i] - second[i] != a:
            bl = False
            break

    if bl:
        f = open('output.txt', 'w')
        f.write('YES' + '\n')
        f.write(str(a))
        f.close()

    else:
        f = open('output.txt', 'w')
        f.write('NO')
        f.close()
