a = []

f = open('input.txt')
n, k = f.read().split()
f.close()

n = int(n)
k = int(k) - 1
b = [int(i) for i in range(n, 0, -1)]

for i in range(n):
    c = b.pop(len(b) - 1)
    if len(a) - 1 >= k:
        a.insert(k, c)
    else:
        a.append(c)

f = open('output.txt', 'w')
for i in range(n):
    f.write(str(a[i]) + ' ')
f.close()
