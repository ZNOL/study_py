n = int(input())
s = input()

mx_y = -1
y = 0
for i in s:
    y += 1 if i == '+' else -1

    if y > mx_y:
        mx_y = y

ans = 0
y = 0
for i in s:
    if y == mx_y:
        ans += 1
    y += 1 if i == '+' else -1

if y == mx_y:
    ans += 1

print(ans)

