n, m = map(int, input().split())

new_c = 0
for i in range(n):
    a, b = map(int, input().split())
    new_c += a
    m -= b

if m <= 0:
    print(0)
else:
    print(m)
