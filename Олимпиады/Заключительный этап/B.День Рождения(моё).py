m = []

n = int(input())
a = list(map(int, input().split()))

if n > 2:
    m.append(a.pop(a.index(max(a))))
    while len(a) > 1:
        m.append(a.pop(a.index(max(a))))
        m.insert(0, a.pop(a.index(max(a))))
    if len(a) == 1:
        m.append(a[0])
    print(*m)
else:
    print(*a)
