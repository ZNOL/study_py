def find(num, seq):
    for i in range(len(seq)):
        for j in range(len(seq[i])):
            if seq[i][j] == num:
                return i, j
    return str(1)


ans = []

n = int(input())
m = []
for i in range(n - 1):
    m += [list(map(int, input().split()))]

for i in range(len(m)):
    a = m[i][0]
    b = m[i][1]
    af = list(find(a, ans))
    bf = list(find(b, ans))
    if af == ['1'] and bf == ['1']:
        ans.append([a, b])
    elif af != ['1'] and bf == ['1']:
        ans[af[0]].insert(af[1], b)
    elif af == ['1'] and bf != ['1']:
        ans[bf[0]].insert(bf[1], a)
    elif af != ['1'] and bf != ['1']:
        ans[af[0]].extend(ans[bf[0]])
        ans[bf[0]].clear()

for i in [i for i in ans if i]:
    print(*i)
