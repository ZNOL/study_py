ans = 0
data = 0

a = int(input())
b = int(input())
c = int(input())
t = int(input())
d = int(input())


for day in range(1, d + 1):
    ans = 0
    for time in range(t):
        if abs(t - (time * a)) == abs(t - (time * b)) == abs(t - (time * c)):
            ans += 1

if a > t * d or b > t * d or c > t * d or a + 1 == b and b + 1 == c:
    print(0)
else:
    print(ans)