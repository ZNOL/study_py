n, m = map(int, input().split())
a = []
for i in range(n):
    a += [list(input())]
ln = list(map(int, '1' * n))
lm = list(map(int, '1' * m))

for whynot in range(5):
    for i in range(n):
        for j in range(m):
            if a[i][j] == '>':
                if not(ln[i] > lm[j]):
                    ln[i] += 1
            elif a[i][j] == '<':
                if not(ln[i] < lm[j]):
                    lm[j] += 1
            elif a[i][j] == '=':
                if ln[i] != lm[j]:
                    ln[i] = lm[j]
z = 0
for i in range(n):
    for j in range(m):
        if a[i][j] == '>':
            if not(ln[i] > lm[j]):
                ln[i] += 1
                z += 1
        elif a[i][j] == '<':
            if not(ln[i] < lm[j]):
                lm[j] += 1
                z += 1
        elif a[i][j] == '=':
            if ln[i] != lm[j]:
                ln[i] = lm[j]
                z += 1
if z != 0:
    print('No')
else:
    print('Yes')
    print(*ln)
    print(*lm)