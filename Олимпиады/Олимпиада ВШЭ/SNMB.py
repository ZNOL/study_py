n = int(input())
m = []
for i in range(n):
    m.append([[int(i) for i in input().split()], i])
m.sort(key=lambda left: left[0])

ans = []
last = [0, 0]
for i in range(n):
    if ans:
        if not(last[0] <= m[i][0][0] and m[i][0][1] <= last[1]):
            if last[1] <= m[i][0][0]:
                ans.append(m[i])
                last = m[i][0]
            elif m[i][0][0] < last[1] < m[i][0][1]:
                ans.append([[last[1], m[i][0][1]], m[i][1]])
                last = [last[1], m[i][0][1]]
            else:
                ans.append([[-1, -1], m[i][1]])
        else:
            ans.append([[-1, -1], m[i][1]])
    else:
        ans.append(m[i])
        last = m[i][0]

for i in sorted(ans, key=lambda index: index[1]):
    print(*i[0])
