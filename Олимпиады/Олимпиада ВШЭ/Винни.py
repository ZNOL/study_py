n = int(input())
a = int(input())
b = int(input())
c = int(input())

d = 0
place = 'k'
for i in range(n - 1):
    if place == 'k':
        if a < b:
            d += a
            place = 'c'
        else:
            d += b
            place = 'i'
    elif place == 'c':
        if a < c:
            d += a
            place = 'k'
        else:
            d += c
            place = 'i'
    else:
        if b < c:
            d += b
            place = 'k'
        else:
            d += c
            place = 'c'

print(d)
