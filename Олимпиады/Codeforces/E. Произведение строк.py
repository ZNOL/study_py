def beauty(a):
    b = 0
    temp = 0
    t = a[0]
    for i in range(len(a)):
        if a[i] == t:
            b += 1
        else:
            if temp < b:
                temp = b
            b = 1
            t = a[i]
    if temp < b:
        temp = b
    return temp


def beauty_one(a):
    length = len(a)
    l = 0
    r = 0
    tl = a[0]
    tr = a[length - 1]
    for i in range(length):
        if a[i] == tl:
            l += 1
        else:
            break
    for i in range(length - 1, -1, -1):
        if a[i] == tr:
            r += 1
        else:
            break
    return l, r


def string(a):
    length = len(a)
    ans = beauty(a)
    l = a[0]
    r = a[length - 1]
    lb, rb = beauty_one(a)
    if ans == length:
        return [ans, l, r, lb, rb, 1]
    else:
        return [ans, l, r, lb, rb, 0]


def skl(s, t):
    s = string(s)
    t = string(t)
    l = s[1]
    r = t[2]
    if s[5] == 1 and s[1] == t[1]:
        lb = s[3] + t[3]
    else:
        lb = s[3]
    if s[5] == 1 and s[2] == t[2]:
        rb = s[4] + t[4]
    else:
        rb = s[4]
    if s[5] == t[5] == 1 and s[1] == t[1]:
        return [0, l, r, lb, rb, 1]
    else:
        return [0, l, r, lb, rb, 0]


n = int(input())

m = []
for i in range(n):
    m.append(input())

print(m)
