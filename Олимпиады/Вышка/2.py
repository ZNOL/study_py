n, k = map(int, input().split())
m = [[0 for i in range(4)] for i in range(n + 2)]

for i in range(1, n + 1):
    m[i][1], m[i][2] = map(int, input().split())
    m[i][3] = m[i][1] + m[i][2]

s = []
for i in range(1, n + 1):
    s.append((m[i][3], i))

s.sort(reverse=True)
for i in range(1, n + 1):
    m[i][1], m[i][2] = 0, 0

for i in range(1, k + 1):
    m[s[i - 1][1]][1], m[s[i - 1][1]][2] = i, i

for i in range(1, n + 1):
    print(m[i][1], m[i][2])


