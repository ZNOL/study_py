#include <iostream>
#include <vector>
using namespace std;

const int SIZE = 100001;

int n, k;
long long s[SIZE];
vector<vector<int> > m;


void find(int a, int b, int &c)
{
    if (a == b){
        c = 1;
        return;
    }   
    for (int i = 1; i <= n; i++)
    {
        if (m[a][i] == 0 and c != 1)
            c = 0;
        if (m[a][i] == 1 and s[i] == 0){
            s[i] = 1;
            find(i, b, c);
        }
    }
}


signed main()
{
    cin >> n >> k;
    m = vector<vector<int> >(n + 1, vector<int>(n + 1));
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= n; j++)        
            m[i][j] = 2;
        
    int x, y, c;
    char com;
    for (int i = 0; i < k; i++)
    {
        cin >> com >> x >> y;
        switch (com)
        {
        case '+':
            m[x][y] = 1;
            m[y][x] = 1;
            break;
        case '-':
            m[x][y] = 0;
            m[y][x] = 0;
            break;
        case '?':
            for (int i = 1; i <= n; i++)
                s[i] = 0;
            c = 2;
            s[x] = 1;
            find(x, y, c);
            if (c == 1)
                cout << '+' << endl;
            else if (c == 2)
                cout << '?' << endl;
            else 
                cout << '-' << endl;
        }
    }

    // system("pause");
}