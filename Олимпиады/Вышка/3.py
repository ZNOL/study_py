def find(a, b):
    global n
    global x
    global y
    global c
    if a == b:
        c = 1
        return
    for i in range(1, n + 1):
        if m[a][i] == 0 and c != 1:
            c = 0
        if m[a][i] == 1 and i not in s:
            s.add(i)
            find(i, b)


n, k = map(int, input().split())

m = [[2 for i in range(n + 1)] for j in range(n + 1)]

for i in range(k):
    com, x, y = input().split()
    x = int(x)
    y = int(y)

    if com == '+':
        m[x][y] = 1
        m[y][x] = 1
    elif com == '-':
        m[x][y] = 0
        m[y][x] = 0
    else:
        s = set()
        c = 2
        s.add(x)
        find(x, y)
        if c == 1:
            print('+')
        elif c == 2:
            print('?')
        else:
            print('-')
