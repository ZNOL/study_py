from itertools import combinations
from math import gcd
# from collections import Counter

mas = []


def factor(n):
    m = []
    d = 2
    while d * d <= n:
        if n % d == 0:
            m.append(d)
            n //= d
        else:
            d += 1
    if n > 1:
        m.append(n)
    return m


def nod_of_many(a):
    se = a[0]
    for i in a[1:]:
        se = gcd(se, i)
    return se


# def nok_of_many(a):
#     se = a[0] * a[1] // gcd(a[0], a[1])
#     for i in range(2, len(a)):
#         se = (se * a[i]) // (gcd(se, a[i]))
#     return se


# def nod_of_three(a):
#     se = []
#     nd = 1
#     for i in range(len(a)):
#         se += [Counter(factor(a[i]))]
#     nod = se[0] & se[1] & se[2]
#     for i in nod.elements():
#         nd *= i
#     return nd


def nok_of_many_s(a):
    se = []
    nok = 1
    for i in a:
        j = factor(i)
        for l in j:
            if l not in se:
                se.append(l)
            elif j.count(l) > se.count(l):
                for k in range(j.count(l) - se.count(l)):
                    se.append(l)
    for i in se:
        nok *= i
    return nok


n = int(input())
a = list(map(int, input().split()))


for i in combinations(a, 3):
    mas += [nod_of_many(i)]


if len(mas) > 1:
    print((nok_of_many_s(mas)) % (10 ** 9 + 7))
else:
    print(mas[0] % (10 ** 9 + 7))
