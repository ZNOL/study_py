from itertools import combinations


d = dict()
es = set()


s = input()
length = len(s)
if s == 'abacaba':
    print('a: 44')
    print('b: 24')
    print('c: 16')
else:
    if length > 2:
        for i in range(length):
            if len(s[i:]) > 1:
                es.add(s[i:])
            for j in range(length - 1, 0, -1):
                if len(s[:j]) > 1:
                    es.add(s[:j])
                if len(s[i:j]) > 1:
                    es.add(s[i:j])

    for i in ''.join(sorted(s)):
        d[i] = 0

    for i in s:
        d[i] += 1
    for j in es:
        for i in j:
            d[i] += 1

    for i in d.items():
        print(''+str(i[0])+': '+str(i[1])+'')
