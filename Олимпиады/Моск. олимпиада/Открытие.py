from sys import stdin

aL = []


def heap_sort(se):
    global ak
    ak = []

    def k(a, b):  # возвращает координаты 2 чисел с индексами a и b
        global m
        global ak
        mas = []
        x, y = divmod(a, m)
        mas.append([x + 1, y + 1])
        x, y = divmod(b, m)
        mas.append([x + 1, y + 1])
        ak += [mas]

    def swap_items(i1, i2):
        global ak
        if se[i1] < se[i2]:
            se[i1], se[i2] = se[i2], se[i1]
            k(i1, i2)

    def sift_down(parent, limit):
        while True:
            child = (parent + 1) << 1
            if child < limit:
                if se[child] < se[child - 1]:
                    child -= 1
                swap_items(parent, child)
                parent = child
            else:
                break

    length = len(se)

    for index in range((length >> 1) - 1, -1, -1):
        sift_down(index, length)

    for index in range(length - 1, 0, -1):
        swap_items(index, 0)
        sift_down(0, index)

    print(len(ak))
    for i in ak:
        print(*i[0], *i[1])


n, m = map(int, input().split())

for i in range(n):
    aL += next(stdin).strip().split()
for i in range(len(aL)):
    aL[i] = int(aL[i])

heap_sort(aL)
