def per(a):
    m = 0
    a = a[::-1]
    for i in range(len(a)):
        if a[i] == "-":
            m -= 3 ** i
        if a[i] == "+":
            m += 3 ** i
    return m


def r_per(a):
    temp = a
    a = abs(a)
    m = []
    c, d = divmod(a, 3)
    m.append(d)
    while c != 0:
        c, d = divmod(c, 3)
        m.append(d)
    m.append(0)
    return persim(m, temp)


def persim(a, temp):
    m2 = []
    if temp >= 0:
        for i in range(len(a)):
            if a[i] == 1:
                m2.append('+')
            elif a[i] == 2:
                m2.append('-')
                a[i + 1] += 1
            elif a[i] == 3:
                m2.append('0')
                a[i + 1] += 1
            elif a[i] == 0:
                m2.append('0')
        m2 = m2[::-1]
        if m2[0] == '0':
            m2 = m2[1:]
        return ('').join(m2)
    else:
        for i in range(len(a)):
            a[i] = -a[i]
        for i in range(len(a)):
            if a[i] == -2:
                m2.append('+')
                a[i + 1] += -1
            elif a[i] == -3:
                m2.append('0')
                a[i + 1] += -1
            elif a[i] == -1:
                m2.append('-')
            elif a[i] == 0:
                m2.append('0')
        m2 = m2[::-1]
        if m2[0] == '0':
            m2 = m2[1:]
        return ('').join(m2)


a = input()


if a.count('(+)') == 1:
    i, q = a.split('(+)')
    i = per(i)
    q = per(q)
    print(r_per(i+q))
elif a.count('(-)') == 1:
    i, q = a.split('(-)')
    i = per(i)
    q = per(q)
    print(r_per(i-q))
