bl = True
a = []
be = []
word = ''


def DFS(ind):
    global a  # массив с буквами
    global m  # кол-во букв в сточке
    global l  # максимальный индекс в массиве
    global word  # слово, которое составляет мой алгоритм
    global str_find  # слово, которое мы пытаемся найти
    global be  # миссив ячеек, в которых мы уже были [индекс ячейки, значение ячейки]
    if word == str_find:
        return True
    if word + str(a[ind]) == str_find[:len(word) + 1]:
        b = ind + 1
        if b > l:
            b = ind
            #
        c = ind + m
        if c > l:
            c = ind
            #
        d = ind - 1
        if d < 0:
            d = ind
        e = ind - m
        if e < 0:
            e = ind
        word += str(a[ind])
        be += [[ind, a[ind]]]
        if ([b, a[b]] not in be) and (word + str(a[b]) == str_find[:len(word) + 1]):
            return DFS(b)
        elif ([c, a[c]] not in be) and (word + str(a[c]) == str_find[:len(word) + 1]):
            return DFS(c)
        elif ([d, a[d]] not in be) and (word + str(a[d]) == str_find[:len(word) + 1]):
            return DFS(d)
        elif ([e, a[e]] not in be) and (word + str(a[e]) == str_find[:len(word) + 1]):
            return DFS(e)
        elif word == str_find:
            return True
        else:
            word = word[:len(word) - 2]
            return DFS(ind + 1)
    elif ind + 1 > l and word != str_find:
        return False
    else:
        return DFS(ind + 1)


n, m = map(int, input().split())

for i in range(n):
    a += list(input())
    #

l = len(a) - 1

length = int(input())
str_find = input()

for i in range(length):
    if str_find[i] not in a:
        bl = False
        break

if n * m < length:
    bl = False

if bl:
    if DFS(0):
        print('YES')
    else:
        print('NO')
else:
    print('NO')

print(be)
