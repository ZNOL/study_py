def num(st):
    global k
    global c
    mas = []
    for i in range(st, n, k):
        mas += [c[i]]
        if len(mas) >= 3:
            break
    if mas.count(1) == 3 or mas.count(1) == 2:
        return 1
    else:
        return 0


def check(ind):
    global n, k, c
    global wrong
    a = num(ind)
    for i in range(ind, n, k):
        if c[i] != a:
            wrong += [i + 1]
        else:
            c[i] = 2
        if len(wrong) >= 2:
            return False
    return True


wrong = []

n, k = map(int, input().split())
c = list(map(int, input().split()))

for i in range(n):
    if c[i] != 2:
        check(i)
    if len(wrong) >= 2:
        break

if not wrong:
    print('OK')
    print(0)
elif len(wrong) == 1:
    print('OK')
    print(wrong[0])
else:
    print('FAIL')
