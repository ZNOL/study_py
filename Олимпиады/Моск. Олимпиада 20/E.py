seq = []
t = int(input())
n = int(input())
seq += [[0, int(input()), int(input()) + t] for i in range(n)]
m = int(input())
seq += [[1, int(input()), int(input()) + t] for i in range(m)]

if n + m == 0:
    print(0)
elif n == 0:
    print(m)
elif m == 0:
    print(n)
else:
    seq.sort(key=lambda x: x[1])
    c = [[int(not(seq[0][0])), seq[0][2]]]

    for i in range(1, n + m):
        a = int(1e5)
        c_train = -1
        for j in range(len(c)):
            if c[j][0] == seq[i][0] and c[j][1] <= seq[i][1]:
                k = abs(seq[i][1] - c[j][1])
                if k < a:
                    a = k
                    c_train = j
        if c_train != -1:
            c[c_train][0] = int(not(c[c_train][0]))
            c[c_train][1] = seq[i][2]
        else:
            c.append([int(not(seq[i][0])), seq[i][2]])

    print(len(c))
    print(seq)
    print(c)