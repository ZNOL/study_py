n = int(input())
m = [int(input()) for i in range(n)]

m.sort(reverse=True)
count = 1
in_row = m[0]
for i in range(1, n):
    if m[i] * (count + 1) > in_row * count:
        in_row = m[i]
        count += 1

print(count, in_row)
