n = int(input())
m = int(input())
t = int(input())

i = 1
a1 = 2 * (n + m - 2)
sum = a1
while True:
    if sum + a1 - 8 > t:
        break
    a1 -= 8
    sum += a1
    i += 1

print(i)
