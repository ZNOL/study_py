from math import floor


def check(t):
    global a
    global b
    global n
    global m
    return (n == floor((t + a) / (1 + a)) or n == floor(t / (1 + a))) and (m == floor((t + b) / (1 + b)) or m == floor(t / (1 + b)))


a = int(input())
b = int(input())
n = int(input())
m = int(input())

ta = n + (n - 1) * a
tb = m + (m - 1) * b
ta1 = n + n * a
tb1 = m + m * b

mn = int(1e5)
mx = -1
for t in range(min(ta, tb, ta1, tb1), max(ta, tb, ta1, tb1) + 1):
    if check(t):
        mn = min(mn, t)
        mx = max(mx, t)

if mn == int(1e5) or mn == -1:
    print(-1)
else:
    print(mn, mx)
