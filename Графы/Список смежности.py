def l_adj(i):
    global m
    ans = []
    a = m[i]
    for j in range(len(a)):
        if a[j] > 0:
            for k in range(a[j]):
                ans.append(j + 1)
    return ans


n = int(input())
m = []
for i in range(n):
    m.append([int(i) for i in input().split()])

for i in range(n):
    print(i + 1, *l_adj(i))
