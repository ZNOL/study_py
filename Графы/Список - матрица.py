def l_mar(i):
    global m
    global n
    ans = []
    a = m[i]
    for j in range(1, n + 1):
        ans.append(a.count(j))
    return ans


n = int(input())
m = []
for i in range(n):
    m.append([int(j) for j in input().split()])
for i in range(n):
    del m[i][0]

for i in range(n):
    print(*l_mar(i))
