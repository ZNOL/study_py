n = int(input())
nr = n - 1
nd = n - 1
nl = 0
nu = 0

ln = [int(i) for i in range(1, (n * n) + 1)]
a = [[0 for i in range(n)] for j in range(n)]

i, j = 0, 0
while len(ln) != 0 and nr >= 0 and nd >= 0 and nl >= 0 and nu >= 0:
    while j < nr and len(ln) != 0:
        a[i][j] = ln.pop(0)
        j += 1
    while i < nd and len(ln) != 0:
        a[i][j] = ln.pop(0)
        i += 1
    while j > nl and len(ln) != 0:
        a[i][j] = ln.pop(0)
        j -= 1
    while i > nu + 1 and len(ln) != 0:
        a[i][j] = ln.pop(0)
        i -= 1
    nr -= 1
    nd -= 1
    nl += 1
    nu += 1
a[i][j] = ln.pop(0)

for i in a:
    print(*i)
