"""
if оператор ветвления

        True
      /
bool -
      \
        False

if <bool значение>:
<tab>a = 120
<tab>print(123523)

=========================================

elif = "else if"

if a < 0:
    do 1
elif a > 0:
    do 2
else:
    do 3

"""

""" Логические операторы

x and y - выполняется оба условия

x or y  - выполняется хотя бы 1 или оба

not x   - выполняет отрицание от х

"""

a = int(input())

if a == 2:
    print('even')
elif a == 3:
    print('odd')
elif a == 3:
    print('even')
else:
    print('xz')

ans = 0
if a > 0:
    ans = ans + 1
if b > 0:
    ans = ans + 1
if c > 0:
    ans = ans + 1

print(ans)