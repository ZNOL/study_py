# def sum2(x: int, y: int) -> str:
#     return x + y
def sum(x, y):
	return x + y

# x = int(input())
# y = int(input())

# print(sum2(x, y))
# print(sum2('2', '5'))
# print(sum2([2], [5]))

def func():
    global x
    print("x in func_st = ", x)
    x = 5
    print("x in func_fn = ", x)
    return 2234723 * x


x = 100
x = func()
print('not in func', x)
