""" Срезы питон

m = [1, 2 ,3 ,4 ,5 ,6 ,7, 8]
s = 'abcdefg'

m[0]  # 1
s[0]  # 1

m[0:3]  # [1, 2, 3] новый массив

y = m[:]  # копия массива (работает для одномерного)  import copy

m[start:finish:step].

По умолчанию start - левая граница
             finish - правая граница
             step == 1


map(func, iterable)

map(int, ['1', '3', '5'])

list(map(int, input().split()))  -  генерирует массив
"""
map()
n = int(input())
print(f'{n:,}')

# print(f'{n:100.6f}')  # "                                                                                       124124.000000"
