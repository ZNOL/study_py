""" ЦИКЛЫ
10^8 операций за секунду

while <bool>:
<tab>
<tab>

for <что-то> in <чём-то>:
<tab>
<tab>

<что-то> - любая переменная
<чём-то> - множество (для начала range(start, finish, step) finish не включает)

"""
"""
# Посчитать арифметическую прогрессию
a = int(input())
d = int(input())  # a2 - a1 = a3 - a2 = ...
n = int(input())  # количество

# print((a1 + an) * n / 2)
a1 = a
an = a + d * (n - 1)

# a1 a2 a3 a4 | a5 a6 a7 a8

# a8 + a1 = a1 + d(n - 1) + a1 = 2a1 + d(n - 1)
# a2 + a7 = a1 + d


print((a1 + an) * n // 2)

s = 0  # итоговая сумма
c = 0  # текущее количество
while c < n:
    s = s + a
    a = a + d

    c = c + 1

print(s)
"""
"""
# Найти Nое число фибонначи

#F{1} = F{2} = 1
# F{i} = F{i - 1} + F{i - 1}

# 1 1 2 3 5 8 13 21 34 . . .

n = int(input())

if n == 1:
    print(1)
elif n == 2:
    print(1)
else:
    f1 = 1
    f2 = 1

    c = 3  # текущий член который можно посчитать сразу
    while True:
        if c == n:
            print(f'Fib_{n} = {f1 + f2}')
            break  # убивает цикл, в котором находится (!)

        tmp = f2  # temporary записали строе значение f2
        f2 = f1 + f2
        f1 = tmp

        # print(f1, f2)

        c = c + 1  # c += 1

"""

# Посчитать сумму квадратов чисел от 1 до n (включительно)

# попробовать найти формулу

n = int(input())

s = 0
for i in range(1, n + 1, 1):  # range(0, n + 1)
    s = s + i * i

print(s)
