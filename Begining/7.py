from math import pow

def power(a : float, n : int):
    # a ^ n = a * a * a * a ... * a
    # бинарное возведение в степень
    if n == 0:
        return 1
    elif n % 2 == 0:  # n четное
        tmp = power(a, n // 2)
        return tmp * tmp
    else:  # n нечетное
        return a * power(a, n - 1)


a, n = input().split()
a = float(a)
n = int(n)

print(power(a, n))
