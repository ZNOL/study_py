n, a = map(int, input().split())
m = []
for i in range(n):
    m.append([[int(i) for i in input().split()], i])

ans = []
bad = []
for i in sorted(m, key=lambda i: i[0][0]):
    if i[0][1] < 0:
        bad.append(i)
    elif i[0][0] <= a:
        a += i[0][1]
        ans.append(i[1] + 1)
for i in sorted(bad, key=lambda i: i[0][1], reverse=True):
    if i[0][0] <= a:
        a += i[0][1]
        ans.append(i[1] + 1)

print(len(ans))
print(*ans)
