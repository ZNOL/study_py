def bin_search(item):
    global m
    mi = 0
    ma = len(m) - 1
    if m:
        if item < m[0]:
            return 0
        elif item > m[-1]:
            return ma
        else:
            while -mi + ma > 1:
                b = (mi + ma) // 2
                if m[b] < item:
                    mi = b
                else:
                    ma = b
            return ma
    else:
        return 0


n = int(input())
m = [int(i) for i in input().split()]
m.sort()

ans = 0
while len(m) > 1:
    i = m.pop(0) + m.pop(0)
    ans += i
    position = bin_search(i)
    if position < len(m) - 1:
        m.insert(position, i)
    else:
        m.append(i)

print(ans)
