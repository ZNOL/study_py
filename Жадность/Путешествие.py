n, k = map(int, input().split())
m = [int(_) for _ in input().split()]
z, m = m[0], m[1:] + [n]

ans = 0
last = k
for i in range(z):
    if last >= m[i]:
        if last >= m[i + 1]:
            continue
        else:
            last = m[i] + k
            ans += 1
    else:
        ans = -1
        break

print(ans)



