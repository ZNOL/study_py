def swap(ind1, ind2):
    global heap
    heap[ind1], heap[ind2] = heap[ind2], heap[ind1]


def sift_up(index):
    global heap
    parent = (index - 1) >> 1
    while index > 0 and heap[parent] > heap[index]:
        swap(index, parent)
        index = parent
        parent = (index - 1) >> 1


def sift_down(parent, limit):
    global heap
    while True:
        child = (parent + 1) << 1
        if child <= limit:
            if child == limit:
                child -= 1
            elif heap[child - 1] < heap[child]:
                child -= 1
            if heap[parent] > heap[child]:
                swap(parent, child)
            parent = child
        else:
            break


def insert(p):
    global heap
    heap.append(p)
    sift_up(len(heap) - 1)


def extract_min():
    global heap
    result = heap[0]
    swap(0, len(heap) - 1)
    del heap[len(heap) - 1]
    sift_down(0, len(heap))
    return result


n = int(input())
heap = [int(i) for i in input().split()]

for i in range((n >> 1) - 1, -1, -1):
    sift_down(i, n)

ans = 0
while len(heap) > 1:
    a = extract_min()
    b = extract_min()
    ans += a + b
    insert(a + b)

print(ans)
