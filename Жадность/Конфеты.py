n = int(input())
a = [[int(i)] for i in input().split()]
c = input().split()
for i in range(n):
    a[i].append(int(c[i]))

sweets = 0
v = 0
for i in sorted(a, key=lambda i: i[0]):
    if v + i[0] <= i[1]:
        v += i[0]
        sweets += 1

print(sweets)