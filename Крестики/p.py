import csv
import requests
from bs4 import BeautifulSoup

main_url = 'https://www.marvel.com/'

r = requests.get(main_url + '/characters')
soup = BeautifulSoup(r.text, features='html.parser')

all_data = []
fields = {'name', 'link'}
for item in soup.find_all('a', {'class': 'explore__link'}):
    href = item.get('href')
    link = main_url + href + '/in-comics'

    tmp = {}

    tmp['link'] = link

    try:
        r = requests.get(link)
        person = BeautifulSoup(r.text, features='html.parser')

        name = person.find('div', {'class': 'masthead__main'}).find('h1').get_text()
        tmp['name'] = name

        for item in person.find_all('li', {'class': 'railBioInfo__Item'}):
            label = item.find('p', {'class': 'railBioInfoItem__label'}).text
            text = item.find('ul', {'class': 'railBioLinks'}).find('li').text
            tmp[label] = text
            fields.add(label)

        all_data.append(tmp)

    except AttributeError:
        pass

with open('output.csv', 'w', encoding='utf-8') as file:
    file_writer = csv.DictWriter(file, delimiter=',', lineterminator='\r', fieldnames=fields)

    file_writer.writeheader()
    for info in all_data:
        file_writer.writerow(info)
