from random import randint


def show(m):
    for i in range(3):
        print('|', end='')
        for j in range(3):
            print(m[i][j], end='|')
        print('\n-------------')


def win(m):
    for i in range(3):
        if m[i][0] == m[i][1] == m[i][2] != ' ':
            return m[i][0]
        elif m[0][i] == m[1][i] == m[2][i] != ' ':
            return m[0][i]
    if m[0][0] == m[1][1] == m[2][2] != ' ' or m[0][2] == m[1][1] == m[2][0] != ' ':
        return m[1][1]
    else:
        return False


def findplace(m, mode):
    l = []
    for i in range(3):
        for j in range(3):
            if m[i][j] == ' ':
                c = (m[(i - 1) % 3][j] == m[(i + 1) % 3][j]) + (m[i][(j - 1) % 3] == m[i][(j + 1) % 3])
                l.append((i, j, c))
    if mode == 0:
        idx = randint(0, len(l) - 1)
    else:
        idx = -1
        tmp_c = -1
        for i in range(len(l)):
            if tmp_c < l[i][2]:
                tmp_c = l[i][2]
                idx = i
    return l[idx][0], l[idx][1]


m = [[' '] * 3 for i in range(3)]


mode = int(input('введите сложность: 0 - простой, 1 - сложный    '))


steps = 9
isFirst = 1
while not win(m) and steps:
    if isFirst:
        x, y = map(int, input().split())
        if 0 <= x <= 2 and 0 <= y <= 2 and m[x][y] == ' ':
            m[x][y] = 'X'
            isFirst = 0
            steps -= 1
    else:
        x, y = findplace(m, mode)
        m[x][y] = '0'
        isFirst = 1
        steps -= 1

    show(m)
    print()

t = win(m)
if t:
    print(t)
else:
    print('draw')
