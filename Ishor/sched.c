/* Дано расписание занятий в формате 
 * sched[][] = {{0,5}, {1,2}, {1,10}}
 * Первое число - время начала, второе - время окончания занятия
 * Найти минимальное число аудиторий, которое позволит проводить 
 * все занятия так, чтобы в одной аудитории было только одно
 * занятие одновременно.
 * 
 * Напр.:
 * sched[][] = {{0,5},{1,2},{6,10}}
 * Ответ: 2
 * 
 * [s,t]: p[s]++ p[t]--
 *
 * 0	1	2	3	4	5	6	7
 * 1	2	2	0	1	2	3	0
 * Ответ 3
 */

#include <stdio.h>
#include <limits.h>
#define MAX 1000001

int min_halls(int lectures[][2], int n)
{
	int p[MAX];
       	for (int i = 0; i<MAX; i++)
		p[i] = 0;
	for (int i = 0; i<n; i++)
	{
		p[lectures[i][0]]++;
		p[lectures[i][1]]--;

	}
	int sum = p[0];
	for (int i = 1; i<MAX; i++)
	{
		p[i] += p[i-1];
		sum = (sum>p[i])?sum:p[i];
	}
	return sum;
}

int main()
{
	int lec[][2] = {
		{0,3},
		{1,3},
		{5,7},
		{4,7},
		{6,7}
	};
	int n = sizeof(lec)/sizeof(lec[0]);
	printf ("%d\n", min_halls(lec, n));
	return 0;
}
