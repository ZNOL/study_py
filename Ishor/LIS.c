/* Дан массив чисел. Найти длину самой длинной возрастающей
 * подпоследовательности (LIS, longest increasing subsequence)
 *
 * Пример:
 * arr[] = {10,22,9,33,21,50,41,60,80}
 * LIS =   {1 ,2, x, 3, x, 4, x, 5, 6} Ответ: 6
 *
 * s[] =   {9,21,33,41,60,80,0,0,0} max_rank = 5
 * ответ max_rank+1
 *
 */
int bin_search(int arr[], int l, int r, int key)
{
	while (r-l > 1)
	{
		int m = l+(r-l)/2;
		if (arr[m] >= key)
			r=m;
		else
			l=m;
	}
	return r;
}

int lis_length(int arr[], int n)
{
	if (n==0) return 0;
	int s[n+1];
	for (int i =0; i<n; i++)
		s[i] = 0;
	int length = 1;
	s[0] = arr[0];
	for (int i = 1; i<n; i++)
	{
		if (arr[i]<s[0]) s[0] = arr[i];
		else if (arr[i]>s[length-1])
			s[length++] = arr[i];
		else
		s[bin_search(s, -1, length-1, arr[i])] = arr[i]; 
	}
	for (int i = 0; i<length; i++)
		printf("s[%d] = %d\n", i, s[i]);
	return length;
}

int main(void)
{
	int arr[] = {10,22,9,33,21,50,41,60,80};
	int n = sizeof(arr)/sizeof(arr[0]);
	return lis_length(arr, n); 

}
