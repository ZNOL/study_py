/* Дан моток проволоки длины n. Также дан массив стоимостей 
 * отрезков проволоки длинами от 1 до n
 * Найти максимальную стоимость, за которую можно сбыть весь моток
 *
 * Напр.:
 * n = 8
 * 1	2	3	4	5	6	7	8	
 * 1	5	8	9	10	17	17	20
 * Ответ: 22
 */
#include <stdio.h>
#include <limits.h>

int max(int a, int b)
{
	return (a>b)?a:b;
}
int cost(int price[], int n)
{
	int val[n+1];
	val[0] = 0;
	int i,j;

	for (i = 1; i<=n; i++)
	{
		int max_val = INT_MIN;
		for (j = 0; j<i; j++)
			max_val = max(max_val, price[j]+val[i-j-1]);
		val[i] = max_val;
	}
	return val[n];
}

int main(void )
{
	int arr[] = {1, 5, 8, 9, 10, 17, 17, 20};
	int size = sizeof(arr)/sizeof(arr[0]);
	printf("%d\n", cost(arr,size));
	return 0;
}
