/*
 * Дан массив из нулей и единиц. Найти минимальное количество
 * перестановок элементов массива, группирующее все единицы
 * вместе.
 *
 * Например:
 * arr[] = {1,0,1,0,1,1} 4
 * 2
 * 2
 * 1
 * {0,1,1,0,0,0}
 *
 * Ответ: 1 (меняем arr[0] и arr[3])
 */

#include <stdio.h>
#include <limits.h>

int min_swaps(int arr[], int n)
{
	int sum = 0;
	for (int i = 0; i<n; i++)
		sum+=arr[i];
	if (sum == n) return 0;
	int wnd = 0;
	int min = INT_MAX;
	for (int i = 0; i<sum; i++)
		wnd+=arr[i];
	for (int i = sum; i<n; i++)
	{
		wnd-=arr[i-sum];
		wnd+=arr[i];
		min = (min<(sum-wnd)) ? min :(sum-wnd);
	}
	return min;

}

int main(void )
{
	int arr[] = {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1};
	int n = sizeof(arr)/sizeof(arr[0]);
	printf("%d", min_swaps(arr, n));
	return 0;
}
