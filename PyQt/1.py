# coding=UTF-8
from PyQt5 import QtWidgets, QtGui, QtCore
from qtable import Ui_MainWindow
import sys

data = ['Python', 'PHP', 'Java']


class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.pushButton.clicked.connect(self.clear)

        self.ui.tableWidget.setRowCount(3)
        self.ui.tableWidget.setColumnCount(2)
        row = 0
        for item in data:
            cellinfo = QtWidgets.QTableWidgetItem(item)
            combo = QtWidgets.QComboBox()
            combo.addItem('Learn')
            combo.addItem('Forget')
            combo.addItem('Delete')

            self.ui.tableWidget.setItem(row, 0, cellinfo)
            self.ui.tableWidget.setCellWidget(row, 1, combo)
            row += 1

    def clear(self):
        self.ui.tableWidget.clear()


app = QtWidgets.QApplication([])
application = MyWindow()
application.show()

sys.exit(app.exec())
