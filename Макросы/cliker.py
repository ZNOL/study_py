# coding=UTF-8

import pyautogui
import time


def dig():
    while True:
        pyautogui.mouseDown(button='left')


def ref(x):
    while True:
        pyautogui.typewrite('1')
        pyautogui.click(button='right')
        pyautogui.typewrite('2')
        pyautogui.mouseDown(button='left')
        time.sleep(x)
        pyautogui.mouseUp(button='left')


def obr():
    pyautogui.typewrite('1')
    while True:
        pyautogui.click(button='left')


def building():
    while True:
        pyautogui.keyDown('shift')


print('1 - копание\n'
      '2 - гравий -> песок\n'
      '3 - песок -> пыль\n'
      '4 - стройка\n'
      '5 - обработка\n')

n = int(input())

time.sleep(8)

if n == 1:
    dig()
elif n == 2:
    ref(0.7)
elif n == 3:
    ref(0.7)
elif n == 4:
    building()
else:
    obr()
